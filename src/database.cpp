#include <iostream>
#include <vector>
#include <exception>
#include <functional>
#include <algorithm>
#include <string_view>
#include <cdlpp/extras.hpp>
#include <cdlpp/bot.hpp>
#include <cdlpp/abstract/env.hpp>
#include "database.hpp"

#define TTTN(type) "TMPL_"+std::to_string(type)+""
using namespace std;


namespace CDL {
const char *Database::TemplateNotMapped::what() const noexcept {
    return "Template enumerated but not mapped";
}
const char *Database::MissingDefaultValue::what() const noexcept {
    return "Column used but has no default value";
}

std::string Database::dbEsc(const std::string& src) {
    std::ostringstream fres;
    for (const auto &character : src) {
        if (character == '\'') {
            fres << "''";
        } else {
            fres << character;
        }
    }
    return fres.str();
}


Database::Database(table_tmpl_getter_t table_tmpl_getter, record_tmpl_getter_t record_tmpl_getter) {
    connection = new asdb::session{*env.aioc};
    connection->open(std::string(env.settings["postgresql"]).c_str(), ec);
    if (ec.failed()) {ec_report(); return;}
    this->table_tmpl_getter = table_tmpl_getter;
    this->record_tmpl_getter = record_tmpl_getter;
    // Try to initialise tables
    initTables();
}

void Database::ec_report(const asdb::result& r) {
    std::cerr << ec.message() << std::endl;
    std::cerr << r.status() << std::endl;
}
void Database::ec_report() {
    std::cerr << ec.message() << std::endl;
}

void Database::initTables() {
    std::cout << "Initialising database..." << std::endl;
    for (auto templ = db_templates::_end - 1; templ != -1; templ--) {
        // Get "template" table
        auto templ_data = table_tmpl_getter(static_cast<db_templates::type>(templ));
        // Check if template exists
        if (templ_data.empty()) {
            throw TemplateNotMapped();
        } else {
            auto table_name = "" TTTN(templ) "";
            // Create basic table
            connection->query(("CREATE TABLE "+table_name+"(ID  TEXT  PRIMARY KEY  NOT NULL);").c_str(),
                              [=](boost::system::error_code const& /*ec*/, asdb::result r) {
                // Check for error
                //if (ec.failed()) {ec_report(r); return;}
                if (r) {
                    // Report success
                    std::clog << " - Table created: " << templ << std::endl;
                }
                // Create each column
                for (const auto& templ_row : templ_data) {
                        connection->query(("ALTER TABLE "+table_name+" ADD COLUMN "+templ_row+';').c_str(),
                                          [=](boost::system::error_code const& /*ec*/, asdb::result r) {
                            // Check for error
                            //if (ec.failed()) {ec_report(r); return;}
                            // Report success
                            if (r) {
                                std::clog << "  - Column created: " << extras::strsplit(templ_row, ' ', 1)[0] << std::endl;
                            }
                        });
                    }
            });
        }
    }
}

void Database::resetCell(const dbid& _id, const std::string& key, const db_templates::type templ, std::function<void (const bool)> cb) {
    // Get record data
    auto templ_data = record_tmpl_getter(templ);
    auto res = templ_data.find(extras::touppers(key));
    // Do query
    if (res == templ_data.end()) {
        throw MissingDefaultValue();
    } else {
        connection->query(("UPDATE " TTTN(templ) " SET "+dbEsc(key)+" = '"+dbEsc(res->second)+"' WHERE ID='"+_id+'\'').c_str(),
                          [=](boost::system::error_code const& ec, asdb::result r) {
            if ((not ec.failed()) and r) {
                cb(false);
            } else {
                ec_report(r);
                cb(true);
            }
        });
    }
}

void Database::get_raw(const dbid& _id, const std::string& key, std::function<void (Row)> cb, const db_templates::type templ) {
    // Find entry
    connection->query(("SELECT "+key+" FROM " TTTN(templ) " WHERE ID='"+_id+'\'').c_str(),
                      [=](boost::system::error_code const& ec, asdb::result r) {
        // Check for error
        if (ec.failed() or not r) {ec_report(r); cb({}); return;}

        // Check if anything could be found
        if (r.num_rows() == 0) {
            connection->query((
                      "INSERT INTO " TTTN(templ) " (ID)"
                      "VALUES ('"+_id+"');"
                     ).c_str(), [=](boost::system::error_code const& ec, asdb::result) {
                // Check for error
                if (ec.failed()) {
                    ec_report(); cb({});
                } else {
                    // Restart function
                    get_raw(_id, key, cb, templ);
                }
            });
        } else {
            // Check if all fields have been found
            if (r.begin()->is_null(0)) {
                // Fill nulled field
                resetCell(_id, key, templ, [=] (const bool error) {
                    if (error) {
                        cb({});
                    } else {
                        get_raw(_id, key, cb, templ);
                    }
                });
            } else {
                // Return row
                cb(*r.begin());
            }
        }
    });
}

void Database::initCellIfRequired(const dbid& _id, const std::string& key, const db_templates::type templ, std::function<void (const bool)> cb) {
    auto &irecs = initialisedRecords;
    if (std::find(irecs.begin(), irecs.end(), _id+key) == irecs.end()) {
        // Not yet initialised, so let's do that!
        get_raw(_id, {key}, [=] (Row r) {
            if (r) {
                initialisedRecords.push_back(_id+key);
                cb(false);
            } else {
                cb(true);
            }
        }, templ);
    } else {
        cb(false);
    }
}

void Database::update(const dbid& _id, const std::string& action, std::function<void (const bool)> cb, const db_templates::type templ) {
    initCellIfRequired(_id, extras::strsplit(action, ' ', 1)[0], templ, [=] (const bool error) {
        // Create safe callback
        std::function<void (const bool)> scb;
        if (cb) {
            scb = cb;
        } else {
            scb = [] (const bool) {};
        }
        // Check for error
        if (not error) {
            // Execute
            connection->query(("UPDATE " TTTN(templ) " set "+action+" where ID='"+_id+'\'').c_str(),
                              [=](boost::system::error_code const& ec, asdb::result r) {
                if (ec.failed() or not r) {
                    ec_report(r); scb(true);
                } else {
                    scb(false);
                }
            });
        } else {
            scb(true);
        }
    });
}

void Database::update(const dbid& _id, const std::string& key, const std::string& value, std::function<void (const bool)> cb, const db_templates::type templ) {
    update(_id, key+" = '"+dbEsc(value)+'\'', cb, templ);
}

void Database::update(const dbid& _id, const std::string& key, bool value, std::function<void (const bool)> cb, const db_templates::type templ) {
    update(_id, key+" = "+std::string(value?"true":"false"), cb, templ);
}
void Database::update(const dbid& _id, const std::string& key, int32_t value, std::function<void (const bool)> cb, const db_templates::type templ) {
    update(_id, key+" = "+std::to_string(value), cb, templ);
}
}
