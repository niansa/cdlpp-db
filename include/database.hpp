#pragma once
#include <string>
#include <utility>
#include <exception>
#include <functional>
#include <optional>
#include "asyncpq.hpp"
namespace asdb = advameg::db::postgres;
#include "db_templates.hpp"


#define to_dbid std::to_string // EVIL!!!!
namespace CDL {
using dbid = std::string;



class Database {
public:
    using Row = std::optional<asdb::result::row>;
    using table_tmpl_getter_t = std::function<const std::vector<std::string> (db_templates::type)>;
    using record_tmpl_getter_t = std::function<const std::map<std::string, std::string> (db_templates::type)>;

private:
    std::vector<dbid> initialisedRecords;
    asdb::session *connection;
    table_tmpl_getter_t table_tmpl_getter;
    record_tmpl_getter_t record_tmpl_getter;

    void initCellIfRequired(const dbid& _id, const std::string &key, const db_templates::type templ, std::function<void (const bool)> cb);
    void get_raw(const dbid& _id, const std::string& key, std::function<void (Row)> cb, const db_templates::type templ);

public:
    boost::system::error_code ec;

    static std::string dbEsc(const std::string& src);

    Database(table_tmpl_getter_t, record_tmpl_getter_t);

    void ec_report(const asdb::result& r);
    void ec_report();
    void initTables();
    void resetCell(const dbid& _id, const std::string& key, const db_templates::type templ, std::function<void (const bool)> cb);
    void update(const dbid& _id, const std::string& action, std::function<void (const bool)> cb = nullptr, const db_templates::type templ = db_templates::_default);
    void update(const dbid& _id, const std::string& key, const std::string& value, std::function<void (const bool)> cb = nullptr, const db_templates::type templ = db_templates::_default);
    void update(const dbid& _id, const std::string& key, bool value, std::function<void (const bool)> cb = nullptr, const db_templates::type templ = db_templates::_default);
    void update(const dbid& _id, const std::string& key, int32_t value, std::function<void (const bool)> cb = nullptr, const db_templates::type templ = db_templates::_default);

    template<typename T>
    void get(const dbid& _id, const std::string& key, std::function<void (T)> cb, const db_templates::type templ = db_templates::_default) {
        get_raw(_id, key, [=] (Row r) {
            cb(r->get<T>(0));
        }, templ);
    }

    struct TemplateNotMapped : public std::exception {
        const char *what() const noexcept;
    };
    struct MissingDefaultValue : public std::exception {
        const char *what() const noexcept;
    };
};
}
